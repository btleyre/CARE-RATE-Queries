"""
Script Description:
Trains a website classifier that uses one of three models.
Can train:
    1. Naive Bayes Classifier
    2. Four three layer feed forward neural networks
    3. Support Vector Machine (Linear Kernel)
"""
import sys
import h5py
import pickle
import sklearn
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.multioutput import MultiOutputClassifier
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping
from sklearn.naive_bayes import GaussianNB

EPOCHS = 15

# Load the data

# File containing the website data
h5f = h5py.File('dat_new_negativetfidf.h5', 'r')

X = h5f["X"][:]
Y = h5f["Y"][:]

h5f.close()

# Partition test and training set.

X, X_test, y, y_test = train_test_split(
    X, Y,
    test_size=0.3333,
    random_state=42
)

if sys.argv[1] == "bayes":
    print("Gaussian Naive Bayes:")
    print("#####################")
    clf = GaussianNB()
    clf = MultiOutputClassifier(clf)
    clf.fit(X, y)
    score = sklearn.metrics.precision_recall_fscore_support(
        y_test,
        clf.predict(X_test),
        average='macro')
    print(score)
    score = sklearn.metrics.precision_recall_fscore_support(
        y,
        clf.predict(X),
        average='macro')

    print(score)
    print(",".join([str(i) for i in score[:3]]))
    with open('bayes.pkl', 'wb+') as f:
        pickle.dump(clf, f)

    print("")

elif sys.argv[1] == "neural":

    print("3 Layer NN:")
    print("#####################")

    early_stop = EarlyStopping(monitor='loss', min_delta=0, patience=3, verbose=1, mode='auto')
    for i in range(4):
        y = y[:, i]

        model = Sequential([
            Dense(min(512, X.shape[1]), input_dim=X.shape[1]),
            Activation('relu'),
            Dense(min(512, X.shape[1])),
            Activation('relu'),
            Dropout(0.2),
            Dense(1),
            Activation('sigmoid')
        ])

        model.compile(optimizer='adam',
                      loss='binary_crossentropy',
                      metrics=['binary_accuracy'])

        print("Fitting Model")
        model.fit(X, y, epochs=EPOCHS, batch_size=32, verbose=1, callbacks=[early_stop])
        predTemp = model.predict_classes(X_test, batch_size=32, verbose=0)
        pred = predTemp
        y_test = y_test[:, i]
        score = sklearn.metrics.precision_recall_fscore_support(
            y_test,
            pred,
            average='macro')

        print(",".join([str(i) for i in score[:3]]))

        model.save("./neuralnet_newNegative_topic%s.h5" % i)

    print("")

elif sys.argv[1] == "svm":
    print("SVM (Linear Kernel):")
    clf = svm.SVC(kernel='linear')
    clf = MultiOutputClassifier(clf)
    clf.fit(X, y)
    predictions = clf.predict(X_test)
    score = sklearn.metrics.precision_recall_fscore_support(
            y_test,
            predictions,
            average='macro')
    print(score)
    score = sklearn.metrics.precision_recall_fscore_support(
            y,
            clf.predict(X),
            average='macro')
    print(score)
    print(",".join([str(i) for i in score[:3]]))
    with open('svcl_less.pkl', 'wb+') as f:
        pickle.dump(clf, f)

    print("")
