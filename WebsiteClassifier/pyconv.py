"""
Script Description:
Given two files containing lists of URLS and corresponding labels, create a json file
with the text from each webpage, as well as the label.
To be used with proc_dump.py .
"""
import urllib
import os
import json
from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}


if not os.path.exists('./convDump'):
    os.makedirs('./convDump')

dp = open('./convDump/newNegativedump.json', 'w+', 2 ** 31 - 1)
dp.write("{\"urls\":[")
cnt = 0

source_file = './LabelsAndUrls\\urls9.txt'
labels_file = './LabelsAndUrls\\labels9.txt'

f = open(source_file, 'r')

urls = f.readlines()

lab = open(labels_file, 'r')

labels = lab.readlines()

f.close()
lab.close()

entries = []

for i in range(len(urls)):

    # Attempt to request HTML from the URL
    url = urls[i]
    req = None
    resp = None
    respData = None
    try:
        req = urllib.request.Request(url, headers=headers)
        resp = urllib.request.urlopen(req)
        respData = resp.read()
    except Exception as e:
        print("Could not open: " + str(url))
        print(e)

    if resp is None or req is None or respData is None:
        continue

    print(url)

    # Extract text from the HTML and write it to the json file.
    soup = BeautifulSoup(respData, "html.parser")

    for script in soup(["script", "style"]):
        script.extract()
    if soup.body is not None and soup.title is not None:
        title = soup.title.string
        if title is not None:
            title = title.strip()
        else:
            title = url
        url = url
        body = soup.body.get_text()

        entry = {
            "doc": {
                "id": cnt,
                "url": url,
                "title": title,
                "body": body
            },
            "label": labels[i].replace("\n", "")
        }
        print("Writing json for: ")
        print(url)
        try:
            if i == len(urls) - 1:
                dp.write("{\"add\":%s}" % json.dumps(entry))
            else:
                dp.write("{\"add\":%s}," % json.dumps(entry))

            cnt += 1
        except Exception as e:
            print("Could not write json for: " + str(url))
            print(e)


# Finish writing to the json file.
try:
    dp.write("],")
    dp.write("\"commit\":{}}")
    dp.close()
except Exception as e:
    print("Could not clean up at the end.")
    print(e)
print("Dumped %d urls to dump.json" % cnt)
print("")
