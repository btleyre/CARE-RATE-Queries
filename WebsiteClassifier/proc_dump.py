"""
Script Description:
Takes a json file produced with pyconv.py and pickles a file that has partitioned the data into two arrays.
One array holds the body of the webpage.
The other holds the labels for that webpage.
"""
import json
import pickle
import re

dir_str = "convDump\\dump"
strs = []
for i in range(7):
    strs.append(dir_str + str(i+1) + ".json")

strs.append('convDump\\newNegativedump.json')


dat = {
    "X": [],
    "Y": []
}

pattern = re.compile(r"(?is)<script[^>]*>(.*?)</script>")
# Prep data for sklearn et al

num_empty = 0
for string in strs:
    with open(string) as f:
        d = json.load(f)

    for doc in d["urls"]:
        if (".js" not in doc["add"]["doc"]["url"]) \
            and (".css" not in doc["add"]["doc"]["url"]) \
            and (".axd" not in doc["add"]["doc"]["url"])\
            and (".jpg" not in doc["add"]["doc"]["url"]) \
            and (".gif" not in doc["add"]["doc"]["url"]) \
            and (".png" not in doc["add"]["doc"]["url"]) \
            and ("_version=" not in doc["add"]["doc"]["url"]) \
                and ("%%3Fversion=" not in doc["add"]["doc"]["url"]):
                    dat["X"].append(re.sub(pattern, '', doc["add"]["doc"]["body"]))
                    dat["Y"].append(doc["add"]["label"])


# Save the data to a pickle file
with open('new_negative_dat.pkl', 'wb') as f:
    pickle.dump(dat, f, protocol=2)
