"""
Script Description:
Takes data from a .pkl file from proc_dump.py and encodes it into one of three feature types.
"""
import sys
import pickle
import h5py
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import gensim


print("Loading dataset...")
with open('new_negative_dat.pkl', 'rb') as f:
    rdat = pickle.load(f)

y = np.array(rdat["Y"])
y = y[np.newaxis]
print(y.shape)
dat = {
    "X": None,
    "Y": None,
}
enc = {
    "label": [],
    "X": None,
}

if sys.argv[1] == "tfidf":
    print("Extracting TF-IDF Features from input...")
    # Vectorize the inputs
    enc["X"] = TfidfVectorizer(ngram_range=(1, 2), max_features=10000)
    enc["X"].fit(rdat["X"])
    dat["X"] = enc["X"].transform(rdat["X"]).toarray()

elif sys.argv[1] == "count":

    print("Extracting Token-Frequencies from input...")
    # Vectorize the inputs
    enc["X"] = CountVectorizer(ngram_range=(1, 2), max_features=10000)
    enc["X"].fit(rdat["X"])
    dat["X"] = enc["X"].transform(rdat["X"]).toarray()

elif sys.argv[1] == "word2vec":
    print("Extracting Sum-of-Word2Vec from input...")
    model = gensim.models.KeyedVectors.load_word2vec_format('C:\\users\\Lanius\\work\\Models\\'
                                                            'GoogleNews-vectors-negative300.bin.gz', binary=True)

    dat["X"] = np.zeros((len(rdat["X"]), 300))
    i = 0
    for doc in rdat["X"]:
        c = 0
        for tok in doc.split():
            if tok in model:
                c += 1
                dat["X"][i] += model[tok]
        if c > 0:
            dat["X"][i] /= float(c)
        i += 1

else:
    print("%s is not an implemented feature type" % sys.argv[1])
    sys.exit(1)

print("Encoding labels...")
# Vectorize the outputs
# Prepare one-hot encoding for each column
t = None

for i in range(y.shape[1]):
    t = np.fromstring(y[0, i], dtype=int, sep=" ")
    print(t)
    t = t[np.newaxis]
    if dat["Y"] is not None:
        dat["Y"] = np.concatenate((dat["Y"], t), axis=0)
    else:
        dat["Y"] = t
    print(dat["Y"])


print("Saving data...")
h5f = h5py.File('dat_new_negative%s.h5' % sys.argv[1], 'w')
for k, v in dat.items():
    h5f.create_dataset(k, data=v)
h5f.close()

with open('enc-%s.pkl' % sys.argv[1], 'wb') as f:
    pickle.dump(enc, f, -1)
