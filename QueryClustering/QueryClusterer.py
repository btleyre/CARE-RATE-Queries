"""
Script Description:
Gives a sample of query clustering with the data produced by LabelProcDump.py
"""

import h5py
import pickle
import numpy as np
import gensim
from sklearn.neighbors import NearestNeighbors

queries = ["Does my partner have dementia", "Dementia Care in my city"]

h5f = h5py.File('LabelData.h5', 'r')

X = h5f["X"][:]
Y = h5f["Y"][:]

print(X.shape)
print(X[2].shape)

print("Loading WORD2VEC")
# Load Word2Vec Model
model = gensim.models.KeyedVectors.load_word2vec_format(
    'C:\\users\\Lanius\\work\\Models\\GoogleNews-vectors-negative300.bin.gz', binary=True)


# Load the encoders
with open('InitialQueriesLabelData.pkl', 'rb') as f:
    words = pickle.load(f)


h5f.close()

# Get mean of word2vec representation of the queries we are getting the neighbours of.

avgs = []
for query in queries:
    c = 0
    avg = np.zeros((1, 300))
    for tok in query.split():
        if tok in model:
            c += 1
            avg += model[tok]
    if c > 0:
        avg /= float(c)

    avgs.append(avg)


# Create NearestNeighbors model.

nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X)

for avg in avgs:

    distances, indices = nbrs.kneighbors(avg)
    print(indices)
    print(indices.shape)
    print(words[indices[0][1]])
    print(Y[indices[0][1]])
    print(words[indices[0][0]])
    print(Y[indices[0][0]])

    print(distances)
