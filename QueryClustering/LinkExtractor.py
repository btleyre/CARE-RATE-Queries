"""
Method to google a single query and return the search results.
"""
import re
import urllib
from bs4 import BeautifulSoup


def google(base_query):
    """

    :param base_query: The query that we will google.
    :type base_query: str
    :return: List of search results.
    :rtype: str []
    """
    print(base_query)

    base_query = base_query.strip()

    base_query = re.sub('\?', '%3F', base_query)

    base_query = re.sub("'", '%27', base_query)

    base_query = re.sub('"', '', base_query)

    base_query = re.sub("â€™", '%27', base_query)

    base_query = re.sub('\s', '+', base_query)

    print(base_query)

    url = 'https://www.google.ca/search?q=' + base_query

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}

    req = None
    resp = None
    respdata = None
    try:
        req = urllib.request.Request(url, headers=headers)
        resp = urllib.request.urlopen(req)
        respdata = resp.read()
    except Exception as e:
        print("Could not open: " + str(url))
        print(e)

    if resp is None or req is None or respdata is None:
        return

    soup = BeautifulSoup(respdata, "html.parser")

    bss = soup.find_all('div', class_='g')

    qurls = []

    for bs in bss:

        h3s = bs.find_all('h3')

        if len(h3s) == 0:

            h3s = bs.find_all('div', class_="r")

            if len(h3s) == 0:
                print('...none found')

                continue

        h3as = h3s[0].find_all('a')

        if len(h3as) == 0:

            h3s = bs.find_all('div', class_="r")

            if len(h3s) == 0:
                print('...none found')

                continue

            h3as = h3s[0].find_all('a')

            if len(h3as) == 0:
                print('...none found')

                continue

        qurl = h3as[0]['href']

        qurls.append(qurl)

    print(qurls)
    return qurls
